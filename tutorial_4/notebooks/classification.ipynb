{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Supervised Learning: Classification of Iris Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### (This notebook file is the modified version of PyCon13 scikit-learn tutorial designed by Jake VanderPlas) jakevdp@cs.washington.edu"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By the end of this section you will\n",
    "\n",
    "- Know how to instantiate a scikit-learn classifier\n",
    "- Know how to train a classifier by calling the `fit(...)` method\n",
    "- Know how to predict new labels by calling the `predict(...)` method\n",
    "\n",
    "In this example we will perform classification of the iris data with several different classifiers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Linear Support Vector Classifier (SVC)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we'll load the iris data as we did before:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from sklearn.datasets import load_iris\n",
    "iris = load_iris()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the iris dataset example, suppose we are assigned the task to guess\n",
    "the class of an individual flower given the measurements of petals and\n",
    "sepals. This is a *classification* task, hence we have:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "X = iris.data\n",
    "y = iris.target\n",
    "\n",
    "print X.shape\n",
    "print y.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the data has this format it is trivial to train a classifier, for instance a support vector machine with a linear kernel:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from sklearn.svm import LinearSVC"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "``LinearSVC`` is an example of a scikit-learn classifier.  If you're curious about how it is used, you can use ``ipython``'s ``\"?\"`` magic function to see the documentation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "LinearSVC?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first thing to do is to create an instance of the classifier.  This can be done simply by calling the class name, with any arguments that the object accepts:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "clf = LinearSVC()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "``clf`` is a statistical model that has parameters that control the learning algorithm (those parameters are sometimes called the *hyperparameters*). Those hyperparameters can be supplied by the user in the constructor of the model. We will explain later how to choose a good combination using either simple empirical rules or data driven selection:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print clf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default the model parameters are not initialized. They will be tuned automatically from the data by calling the ``fit`` method with the data ``X`` and labels ``y``:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "clf = clf.fit(X, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now see some of the fit parameters within the classifier object.\n",
    "\n",
    "**In scikit-learn, parameters defined by training have a trailing underscore.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "clf.coef_\n",
    "# shape of clf.coef_ [n_classes x n_features] (coefficients are the weights used in inner product)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "clf.intercept_\n",
    "# shape of clf.intercept [n_classes x 1] (Constants in decision function.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the model is trained, it can be used to predict the most likely outcome on unseen data. For instance let us define a list of simple sample that looks like the first sample of the iris dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "X_new = [[ 5.0,  3.6,  1.3,  0.25]]\n",
    "print clf.predict(X_new)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Support Vector Classifier with different kernels"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from matplotlib import colors as c\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from sklearn import svm\n",
    "%matplotlib inline\n",
    "\n",
    "X = iris.data[:,2:4]  # we only take the two features.\n",
    "y = iris.target\n",
    "h = .02  # step size in the mesh\n",
    "\n",
    "# we create an instance of SVM and fit out data. We do not scale our\n",
    "# data since we want to plot the support vectors\n",
    "C = 1.0  # SVM regularization parameter\n",
    "svc = svm.SVC(kernel='linear', C=C).fit(X, y)\n",
    "rbf_svc = svm.SVC(kernel='rbf', gamma=0.7, C=C).fit(X, y)\n",
    "poly_svc = svm.SVC(kernel='poly', degree=3, C=C).fit(X, y)\n",
    "lin_svc = svm.LinearSVC(C=C).fit(X, y)\n",
    "\n",
    "# create a mesh to plot in\n",
    "x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1\n",
    "y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1\n",
    "xx, yy = np.meshgrid(np.arange(x_min, x_max, h),\n",
    "                     np.arange(y_min, y_max, h))\n",
    "\n",
    "# title for the plots\n",
    "titles = ['SVC with linear kernel',\n",
    "          'LinearSVC (linear kernel)',\n",
    "          'SVC with RBF kernel',\n",
    "          'SVC with polynomial (degree 3) kernel']\n",
    "\n",
    "cMap = c.ListedColormap(['#008aff','#ff7e00','#00b756'])\n",
    "\n",
    "plt.figure(figsize=(16,10))\n",
    "for i, clf in enumerate((svc, lin_svc, rbf_svc, poly_svc)):\n",
    "    # Plot the decision boundary. For that, we will assign a color to each\n",
    "    # point in the mesh [x_min, m_max]x[y_min, y_max].\n",
    "    plt.subplot(2, 2, i + 1)\n",
    "    plt.subplots_adjust(wspace=0.1, hspace=0.25)\n",
    "\n",
    "    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])\n",
    "\n",
    "    # Put the result into a color plot\n",
    "    Z = Z.reshape(xx.shape)\n",
    "    plt.contourf(xx, yy, Z, cmap=cMap, alpha=0.5)\n",
    "\n",
    "    # Plot also the training points\n",
    "    plt.scatter(X[:, 0], X[:, 1], s=80, c=y, cmap=cMap)\n",
    "    plt.xlabel('Sepal length (first feature)',fontsize=15)\n",
    "    plt.ylabel('Sepal width (second feature)',fontsize=15)\n",
    "    plt.xlim(xx.min(), xx.max())\n",
    "    plt.ylim(yy.min(), yy.max())\n",
    "    plt.xticks(())\n",
    "    plt.yticks(())\n",
    "    plt.title(titles[i],fontsize=15)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise: Using a Different Classifier ( e.g. Logistic Regression )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we'll take a few minutes and try out another learning model.  Because of ``scikit-learn``'s uniform interface, the syntax is identical to that of ``LinearSVC`` above.\n",
    "\n",
    "There are many possibilities of classifiers; you could try any of the methods discussed at <http://scikit-learn.org/stable/supervised_learning.html>.  Alternatively, you can explore what's available in ``scikit-learn`` using just the tab-completion feature.  For example, import the ``linear_model`` submodule:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from sklearn import linear_model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And use the tab completion to find what's available.  Type ``linear_model.`` and then the tab key to see an interactive list of the functions within this submodule.  The ones which begin with capital letters are the models which are available."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "linear_model.LogisticRegression?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now select a new classifier and try out a classification of the iris data.\n",
    "\n",
    "Some good choices are\n",
    "\n",
    "- ``sklearn.naive_bayes.GaussianNB`` :\n",
    "    Gaussian Naive Bayes model. This is an unsophisticated model which can be trained very quickly.\n",
    "    It is often used to obtain baseline results before moving to a more sophisticated classifier.\n",
    "\n",
    "- ``sklearn.svm.LinearSVC`` :\n",
    "    Support Vector Machines without kernels based on liblinear\n",
    "\n",
    "- ``sklearn.svm.SVC`` :\n",
    "    Support Vector Machines with kernels based on libsvm\n",
    "\n",
    "- ``sklearn.linear_model.LogisticRegression`` :\n",
    "    Regularized Logistic Regression based on liblinear\n",
    "\n",
    "- ``sklearn.linear_model.SGDClassifier`` :\n",
    "    Regularized linear models (SVM or logistic regression) using a Stochastic Gradient Descent algorithm written in Cython\n",
    "\n",
    "- ``sklearn.neighbors.NeighborsClassifier`` :\n",
    "    k-Nearest Neighbors classifier based on the ball tree datastructure for low dimensional data and brute force search for high dimensional data\n",
    "\n",
    "- ``sklearn.tree.DecisionTreeClassifier`` :\n",
    "    A classifier based on a series of binary decisions.  This is another very fast classifier, which can be very powerful.\n",
    "\n",
    "Choose one of the above, import it, and use the ``?`` feature to learn about it. \n",
    "\n",
    "Some models have additional prediction modes. For example, if ``clf`` is a ``LogisticRegression`` classifier, then it is possible to do a probibilistic prediction for any point.  This can be done through the ``predict_proba`` function:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now instantiate this model as we did with ``LinearSVC`` above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "clf2 = linear_model.LogisticRegression(C=1e5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now use our data ``X`` and ``y`` to train the model, using the ``fit(...)`` method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "X = iris.data[:,2:4]\n",
    "y = iris.target\n",
    "\n",
    "clf2.fit (X, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now call the ``predict`` method, and find the classification of ``X_new``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "X_new = [[3.6, 0.25]]\n",
    "print clf2.predict_proba(X_new)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The result gives the probability (between zero and one) that the test point comes from any of the three classes.\n",
    "\n",
    "This means that the model estimates that the sample in X_new has:\n",
    "\n",
    "- 8.5% likelyhood to belong to the ‘setosa’ class (``target = 0``)\n",
    "- 91% likelyhood to belong to the ‘versicolor’ class (``target = 1``)\n",
    "- < 1% likelyhood to belong to the ‘virginica’ class (``target = 2``)\n",
    "\n",
    "Of course, the predict method that outputs the label id of the most likely outcome is also available:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Polynomial features\n",
    "#from sklearn.preprocessing import PolynomialFeatures\n",
    "\n",
    "#poly = PolynomialFeatures(degree=2)\n",
    "#X2 = poly.fit_transform(X)\n",
    "\n",
    "#clf2.fit (X2, y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from matplotlib import colors as c\n",
    "\n",
    "# Plot the decision boundary. For that, we will assign a color to each\n",
    "# point in the mesh [x_min, m_max]x[y_min, y_max].\n",
    "x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5\n",
    "y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5\n",
    "xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))\n",
    "\n",
    "#Change for polynomial features\n",
    "Z = clf2.predict(np.c_[xx.ravel(), yy.ravel()])\n",
    "Zprob = clf2.predict_proba(np.c_[xx.ravel(), yy.ravel()])\n",
    "#Z = clf2.predict(poly.fit_transform(np.c_[xx.ravel(), yy.ravel()]))\n",
    "#Zprob = clf2.predict_proba(poly.fit_transform(np.c_[xx.ravel(), yy.ravel()]))\n",
    "\n",
    "# Put the result into a color plot\n",
    "Z = Z.reshape(xx.shape)\n",
    "Zprob = Zprob.reshape(xx.shape[0],xx.shape[1],3)\n",
    "\n",
    "labels = ['class 1', 'class 2', 'class 3']\n",
    "\n",
    "# number of classes and plot colors\n",
    "n_classes = np.amax(y)+1\n",
    "plot_colors = ['#008aff','#ff7e00','#00b756']\n",
    "\n",
    "cMap = c.ListedColormap(['#008aff','#ff7e00','#00b756'])\n",
    "plt.figure(figsize=(8,5))\n",
    "plt.contourf(xx, yy, Z, cmap=cMap, alpha=0.5)\n",
    "\n",
    "# Plot also the training\n",
    "for j, color in zip(range(n_classes), plot_colors):\n",
    "    idx = np.where(y == j)\n",
    "    plt.scatter(X[idx, 0], X[idx, 1], s=100, c=color, label=labels[j], cmap=plt.cm.Paired)\n",
    "\n",
    "plt.xlabel('Sepal length', fontsize=15)\n",
    "plt.ylabel('Sepal width', fontsize=15)\n",
    "\n",
    "plt.xlim(xx.min(), xx.max())\n",
    "plt.ylim(yy.min(), yy.max())\n",
    "plt.xticks(())\n",
    "plt.yticks(())\n",
    "plt.title('Logistic regression (LR)',fontsize=15)\n",
    "plt.legend(loc='upper left')\n",
    "plt.show()\n",
    "\n",
    "titles = ['LR class 1 - probabilities',\n",
    "          'LR class 2 - probabilities',\n",
    "          'LR class 3 - probabilities']\n",
    "\n",
    "plt.figure(figsize=(18,3))\n",
    "for i, boundaries in enumerate((Zprob[:,:,0], Zprob[:,:,1], Zprob[:,:,2])):\n",
    "    plt.subplot(1, 3, i + 1)\n",
    "    plt.subplots_adjust(wspace=0.1, hspace=0.25)\n",
    "    plt.pcolormesh(xx, yy, boundaries, cmap=plt.cm.jet)\n",
    "    plt.colorbar()\n",
    "\n",
    "    # Plot also the training\n",
    "    for j, color in zip(range(n_classes), plot_colors):\n",
    "        idx = np.where(y == j)\n",
    "        plt.scatter(X[idx, 0], X[idx, 1], s=100, c=color, label=labels[j])\n",
    "    \n",
    "    plt.xlabel('Sepal length', fontsize=15)\n",
    "    plt.ylabel('Sepal width', fontsize=15)\n",
    "\n",
    "    plt.xlim(xx.min(), xx.max())\n",
    "    plt.ylim(yy.min(), yy.max())\n",
    "    plt.xticks(())\n",
    "    plt.yticks(())\n",
    "    plt.title(titles[i],fontsize=15)\n",
    "    plt.legend(loc='upper left')\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluating the Model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Predicting a new value is nice, but how do we guage how well we've done?\n",
    "We'll explore this in more depth later, but here's a quick taste now.\n",
    "\n",
    "Let's get a rough evaluation our model by using\n",
    "it to predict the values of the training data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "y_model = clf2.predict(X)\n",
    "print y_model == y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that most of the predictions are correct!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Model Evaluation Using Cross-Validation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's try 10-fold cross-validation, to see if the accuracy holds up more rigorously."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from sklearn.cross_validation import cross_val_score\n",
    "# evaluate the model using 10-fold cross-validation\n",
    "scores = cross_val_score(linear_model.LogisticRegression(C=1e2), X, y, scoring='accuracy', cv=10)\n",
    "print scores\n",
    "print scores.mean()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
